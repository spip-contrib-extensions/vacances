<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// V
	'vacances_description' => '',
	'vacances_nom' => 'Vacances',
	'vacances_slogan' => 'Permet d\'activer le mode vacances',
);
