<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// A
	'alerte_mode_active' => 'Le mode vacances est activé',
	'alerte_mode_modifier' => 'modifier',

	// C
	'cfg_exemple' => 'Exemple',
	'cfg_exemple_explication' => 'Explication de cet exemple',
	'cfg_titre_parametrages' => 'Configurer le mode vacances',
	'cfg_mode_vacances' => 'Activer le mode vacances',
	'cfg_date_debut' => 'Date de début',
	'cfg_date_fin' => 'Date de fin',
	'cfg_date_fin_explication' => 'Indiquer la date de fin de vacances',
	'cfg_dates_explication' => 'A titre indicatif,  vous pouvez aussi indiquer les dates précises de vos vacances.',

	// M
	'mode_vacances' => 'Mode vacances',

	// T
	'titre_page_configurer_vacances' => 'Vacances',

	// V
	'vacances_titre' => 'Vacances',
);
